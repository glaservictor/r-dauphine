rm(list = ls())

#Question 1
#f est une densite sur R ssi son integrale sur R vaut 1
#On a alors:
#Le calcul de l'intégral de f(x)dx <=> C*[arctan]_{-1}^{1} = C*2*arctan(1)
# arctan(1) = pi/4 | C*2*arctan(1) = 1 <=> C = 2/pi
#Conclusion:  C=2/pi

#Question 2
#D'apres ce qui a ete demontre en TD, G(U) suit la meme loi que la variable aleatoire X


#question 3
#Comme X admet f pour densite, alors la fonction de repartition de X est:
#Pour tout x dans R
#           0 si x< -1
#F(x)=      (arctan(x) + arctan(1))*2/pi si -1 <= x <= 1
#           1 si x> 1

F <- function(x) {
  if (x < -1) { return(0) }
  if (x > 1) { return(1) }
  return((atan(x) + pi/4) * 2 /pi)
}

G <- function(F, y) {
  if (y <= 0) { return(-1) }
  if (y >= 1) { return(1) }
  return (tan((y - 1/2)*pi/2))

  a <- -1
  b <- 1
  x <- (b + a) / 2
  epsilon <- 10 ^ -10
  while (abs(F(x) - y) > epsilon) {
    if (F(x) > y) {
      b <- x
      x <- (b + a) / 2
    }
    else {
      a <- x
      x <- (b + a) / 2
    }
  }
  return(x)
}

vG <- Vectorize(G, 'y')

n <- 1000000
tirage <- runif(n, 0, 1)
res <- vG(F, tirage)
h <- hist(res, breaks = 50, freq = FALSE)
par(new = TRUE)

f <- function(x) {
  return(2/(pi*(1+x^2)))
}
x <- seq(-1, 1, length = 1000)
y <- f(x)

lines(x, y, type = "l",col="red")
print(h)

#1000 c'est un peu petit
#Commentaire a revoir