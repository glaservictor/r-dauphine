rm(list=ls())

#Question 1
tirage <- c(rexp(2000,2))

#Question 2
bords<-0:50
h<-hist(tirage,breaks=bords,freq=FALSE)
x<-2*exp(-2*bords)
plot(h, freq=FALSE)

#Question 3
par(new=TRUE)
lines(bords,x,col='red')

#Question 4
n<-1:2000
#On attribue a x le vecteur contenant la famille de points definie
x<-1/n*cumsum(tirage)
plot(approx(x,n=2000,method="linear"),type = 'l',xlab="x",ylab="y")
