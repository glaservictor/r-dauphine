rm(list=ls())

f <- function(x) {
    return (abs(cos(2*x)))
}
f(-3*pi/4)


somme_riemann<- function(n){
    u<-seq(from = 0,to = pi - pi/n, length=n)
    v <- (pi/n)*sum(f(u))
    return (v)
}

somme_riemann(10)
somme_riemann(100)
somme_riemann(1000)


#Somme i=0,...,n-1 (b-a)/n f(a+i*b/n) = Integrale sur [a, b] de f

#Question 2
#Pour n=10   : 2.033281
#Pour n=100  : 1.999342
#Pour n=1000 : 1.999993


#Question 3: 
#Par un calcul explicite de l'integrale de f entre 0 et pi/2, on obtient 2 coomme valeur de l'integrale

#Calcul des écarts relatifs:
p1 <- abs(2- somme_riemann(1000))/2
p2 <- abs(2- somme_riemann(100))/2
p3 <- abs(2- somme_riemann(10))/2
