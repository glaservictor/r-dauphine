rm(list=ls())

#Le de est pipe: p1=1/2 et p2=p3=p4=p5=p6
#Par un calcul, on a : 5 * p2 = 1 - 1/2 = 1/2
#On en deduit que p2=p3=p4=p5=p6 = 1/10

#Question 1 
x = 1:6
p = c(1/2,rep(1/10,5))
tirage <- sample(x, 1000, replace = TRUE, prob = p)

#Question 2
bords<-seq(0.5,6.5,1)
h<-hist(tirage,breaks=bords,freq=FALSE)
print(h)


#Question 3
n <- 1:1000
x <- 1/n*cumsum(tirage)
plot(n,x,main="Interpolation lineaire",xlab="x",ylab="y",type = 'l',col='red')

#On remarque que lorsque n est "grand", la moyenne des valeurs tend vers 2.5

#Il au aussi possible de calculer l'esperance attentue
#En notant X la variable aleatoire correspondant au resultat d'un tirage
#On a : E[X] = 1/2*1 + 1/10*(2+3+4+5+6) = 2.5
#Ce qui correspond bien au resultat empirique observe
