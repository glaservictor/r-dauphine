rm(list=ls())

theta <- rnorm(1)

#Question 1

# Soit a,b deux reels
# On note (*) la propriete : P(U ∈[a, b]) >= 0.98

# Soit alpha, beta deux reels 
# Si (alpha,beta) respecte (*), alors 
# P(alpha <= U <= beta) = P(U <= beta) - P(U < alpha)
# Alors P(U <= beta) >= 0.98 + P(U < alpha) > 0.98
# Et donc  0 < P(U < alpha) <= P(U <= beta) - 0.98
#On obtient deux conditions 
#  0 < P(U < alpha) <= P(U <= beta) - 0.98
#  P(U <= beta) > 0.98

#Rappel : comme U est une v.a.r a densite, alors pour tout x reel, on a
# P(U<=x) = P(U<x)
# On cherche alors beta tel que P(U <= beta) = 0.99
# Et alpha tel que P(U<=alpha) = 0.005 < 0.99-0.98

b <- qnorm(0.99)
a <- qnorm(0.005)
#On obtient 
# a = -2.575829
# b = 2.326348
# Et on a pnorm(b) - pnorm(a) = 0.985 >= 0.98

# Unicité du couple: Non
# Si (a,b) est un couple tel que (*) est verifiee, alors (a-1, b+1) verifie aussi (*) car [a,b] C [a-1, b+1], 
#D'ou P(U ∈[a-1, b+1]) >= P(U ∈[a, b]) >= 0.98
#Donc il n'existe pas un unique couple.

#Recherche du couple (alpha, beta) optimal
# Comme cela a ete vu en cours, pour avoir un couple optimal, il faut que l'intervalle 
# soit centre en 0, c'est a dire que beta0 = -alpha0, avec alpha0<0
# Mais alors il faut que P(U<alpha0) + P(U>beta0) = P(U<alpha0) +P(U>-alpha0) <= 0.02
#Comme U suit une loi normale centree reduite, on a pour tout reel x, P(U <= x) = P(U >= -x)
#Donc on doit prendre alpha0 tel que P(U<alpha0) <= 0.01

alpha <- qnorm(0.01)
beta <- -alpha

#On obtient (alpha0,beta0) = (-2.326348,2.326348)

#Question 2
#Soit (X1, ...,Xn) des realisations de la loi N(theta, 1)
#Notons Y_n = 1/n * Somme_{i=1}^{n}(X_i)
# Soit Z_n = (Y_n - theta) * sqrt(n)
# D'apres le theoreme de la limite centrale, Z_n converge en loi vers 
# loi normale N(0,1)
#Toujours d'apres le cours, si a<b, alors 
#P(a<=Z_n<=b) -> P(a<= N(0,1) <= b) quand n->infini
# De plus comme les X_i sont des variables aleatoire de loi normale N(theta,1),
# Alors on a l'egalite (en loi) suivante (d'apres le cours):
 #P(a<=Z_n<=b) = (loi) P(a<= N(0,1) <= b) 


# On a alors 
# P(alpha0 <= Z_n <= beta0) = P(Y_n - beta0/sqrt(n) <= theta <= Y_n - alpha0/sqrt(n))
#                           = P(alpha0<= U <= beta0) 
# On cherche alors un intervalle I tel que P(theta dans I) >= 0.98 pour tout theta
# En utilisant les resultats de la question precedente
# On prend I = [Y_n - beta0/sqrt(n), Y_n - alpha0/sqrt(n)]
# Et on a bien P(theta dans I) >= 0.98


n <- 100
X <- rnorm(n, theta, 1) #Ensemble des realisation d'un tirage des (X_n)
mu <- mean(X) #Moyenne du tirage
#On rappelle que l'esperance de chacun des X_n est theta (loi normale de parametres (theta,1))
#intervalle trouve: 

intervalle <- function(n,theta){
  X <- rnorm(n, theta, 1) #Ensemble des realisation d'un tirage des (X_n)
  mu <- mean(X)
  I <- c(mu-beta/sqrt(n), mu-alpha/sqrt(n))
  return(I)
}
I <- intervalle(n,theta)
cat("Intervalle trouve: [",I[1],",",I[2],"]")

m <- 100

testIntervalle <- function(n,theta) {
  X <- rnorm(n, theta, 1) #Ensemble des realisation d'un tirage des (X_n)
  mu <- mean(X)
  # P(theta ∈ I) >= 0.98
  I <- c(mu-beta/sqrt(n), mu-alpha/sqrt(n))
  cat("Intervalle trouve: [",I[1],",",I[2],"]\n")
  inI <- (theta >= I[1]) && (theta <= I[2])
  return (inI)
}


param <- rep(theta,m)
finalTest <- Vectorize(testIntervalle,'theta')
re <- finalTest(n,param)
res <- sum(re)
cat("Nombre de fois ou theta est dans l'intervalle trouve:", res,"\n")
cat("Proportion de reussite:", res/m,"\n")


