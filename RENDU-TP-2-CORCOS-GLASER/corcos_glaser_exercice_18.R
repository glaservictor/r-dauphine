rm(list=ls())

#Question 1 
n <- 500
tirage <- rpois(n*n,3)
X <- matrix(tirage, nrow=n)

#Question 2
# E[M_j,n] = 1/n Somme_(i=1)^n E[X_i,j] = 1/n * n * 3
# Ou on a utilise la linearite de l'esperance et que pour tout 1<=i,j<=500, E[X_i,j] = 3

#Determinons a et b tels que Les variables T(j,n) soient centrees et reduites

# E[T(j,n)] = 0 
# <=> E[T(j,n)] = sqrt(n) * (E[M(j,n)] - a)/b = 0  par linearite de l'esperance
# <=> 0 = sqrt(n)*(3 - a)/b 
# <=> a = 3

# Les X_i,j sont independantes et suivent une loi de poisson de paramètre 3 donc 
# Pour tout 1<=i,j<=500, V[X_i,j] = 3

# V[M(j,n)] = V[1/n*somme_{i=1}^n X_i,j] = 1/n^2 * V[somme X_i,j] = 1/n^2*somme V[X_i,j] = 3/n
# Ou on a utilise que V(aX+b) = a^2 V(X) 

# De plus: V[T(j,n)] = V[sqrt(n)*(M(j,n) - a)/b]

# Ainsi : 
# V[T(j,n)] = 1
# <=> V[sqrt(n) * (M(j,n) - 3)/b]  = 1
# <=> sqrt(n)^2 * 1/b^2 * V[M(j,n)] = 1
# <=> 1 = n/(b^2) * (3/n)
# <=> b = sqrt(3)
a1 <- 3
b1 <- sqrt(3)

#On obtient alors que T(j,n) est centree reduite ssi a = 3 et b = sqrt(3)

#Question 3
M<-function(X,j,n){
    return (1/n * sum(X[1:n,j]))
}

T<-function(X,j,n,a,b){
    #On prend les valeurs de a et b calculees precedemment
    return (sqrt(n) * (M(X,j,n)-a)/b)
}

j <- 1:n
vT <- Vectorize(T,"j")
tirage_TP <- vT(X,j,n,a1,b1) 
#Le P permet de signifier d'un tirage de T  avec une loi de Poisson

bords <- seq(-4.5,4.5,length=sqrt(n))

h<-hist(tirage_TP,breaks=bords,main='Histogramme pour une loi de Poisson',freq=FALSE)
par(new = TRUE)

x <- seq(-4.5,4.5,length = n)
lines(x,dnorm(x),type="l",col="red")
# On constate que l'histogramme se rapproche de la courbe de la loi normale 
# centree reduite lorsque n->infini
# Cet exercice vient illustrer le theoreme de la limite centrale

#Question 4


tirage_U <- runif(n*n) #Par defaut les parametres sont 0 et 1
U <- matrix(tirage_U, nrow=n)
# Pour une loi U_i,j uniforme sur [0,1] on a E[U_i,j] = 1/2, V[U_i,j] = 1/12
# Par le meme raisonnement que pour la question 2, on obtient 
#(on ne reecrit pas les calculs pour ne pas alourdir le code )
# E[M(j,n)] = 1/2 et V[M(j,n)] = 1/(12 * n)
# Ainsi, a = 1/2 et b = sqrt(3)/6 
a2 <- 1/2
b2 <- sqrt(3)/6
j <- 1:n
bords <- seq(-4.5,4.5,length=sqrt(500))

tirage_TU <- vT(U,j,n,a2,b2) 
h<-hist(tirage_TU,breaks=bords,main='Histogramme pour une loi Uniforme',freq=FALSE)
par(new = TRUE)
lines(x,dnorm(x),type="l",col="red")
# De la meme maniere, l'histogramme se rapproche de la courcbe de la loi normale centree reduite
