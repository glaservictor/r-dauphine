rm(list=ls())

#Question 2

#Soit X une v.a. de densite f(x)= C exp(-x^2) 1[0,2](x)

# Soit g:R->R la densite d'une loi uniforme sur [0,2]
# Pour tout reel x: g(x) = 1/2 * 1[0,2](x)

# Posons c = 2*C et montrons que c verifie les conditions suivantes:
#   (a) c >= 1
#   (b) Pour tout reel x, f(x)<=g(x)

#Montrons (a)
#Comme f est une densite, alors C>0 
# Si C<0 alors f est negative (or f est une densite donc absurde)

# Si C = 0 alors l'integrale de f sur R est nulle
#ce qui est aussi impossible car f est une densite

#De plus, par decroissance sur [0,+infini[ de la fonction u|->exp(-u^2)
# On a pour tout x dans [0,2], exp(-x^2) <= 1
# En integrant sur [0,2], on obtient alors:
# Integrale_R (f(x) dx) = Integrale_{0}^{2} (f(x) dx)= 1 <= C * Integrale_{0}^{2}dx = C*2
# D'ou C >= 1/2

#On a donc bien c = 2*C >= 1


# Montrons (b)
# On a pour tout x reel:
# f(x)  = C*exp(-x^2) 1{[0,2]}(x) 
#       <=  2*C * 1/2 * 1[0,2](x) Car exp(-x^2) <= 1 pour tout x reel
#       <=  c * 1/2*1[0,2](x)
#       =   c * g(x) 



# On definit pour tout reel x: h(x) = f(x)/(c g(x)) * 1{g(x)>0}
#Soit (Yn){n>=1} une famille de v.a.r. de densite g.
#Soit (Un){n>=1} Une famille de v.a.r. uniformes sur [0,1]
#On suppose que la famille (Y1,Y2,...,U1,U2,...) est mutuellement independante


g<-function(x){
    if (x<0 || x>2){
        return(0)
    }
    else{
        return(1/2)
    }
}

#Definissons la fonction h de maethode du rejet:
# h(x)  = f(x)/[ c*g(x) ] = (C*exp(-x^2))/(2*C*1/2) * 1[0,2](x) 
#       = exp(-x^2) * 1[0,2](x)

h <- function(x){
    if(x < 0 || x > 2){
        return (0)
    }else {
       return (exp(-x^2))
    }
}

rejet <- function(size){
    n<-1
    Y<-runif(1,0,2)
    U<-runif(1,0,1)
    while (U>h(Y)){
        n<-n+1
        Y<-runif(1,0,2)
        U<-runif(1,0,1)
    }
    X<-Y
    return(X)
}

#Question 3
n<-1000 #Taille de l'echantillon
echantillon <- Vectorize(rejet)

e <- echantillon(1:n)
nb_classes <- 50
#On represente sur [0,2] car la fonction de densite est nulle partout ailleurs
bords <- seq(0,2,length = nb_classes)

histogram <- hist(e,breaks=bords,freq=FALSE,plot=TRUE)


#Question 4

p <- function(x) {
    if (x < 0 || x > 2){
        return(0)
    }
    return (exp(-x^2))
}

integrale<-integrate(p,0,2)
C <- 1/(integrale$value)
#Pour trouver la 'valeur' de l'integrale, voici le site utilise
#https://stackoverflow.com/questions/17261092/saving-integration-values-in-an-array-in-r#

f <- function(x,C) {
    if (x < 0 || x > 2){
        return (0)
    }else {
        return (C*exp(-x^2))       
    }
}


x <- seq(0,2,length=1000)
y <- f(x,C)
lines(x,y,type='l',col='red')

#Commentaires sur les resultats obtenus:
#La courbe obtenue coincide presque partout avec les sommets des 
#cases de l'histogramme
# La methode du rejet permet d'approcher facilement la loi 
# d'une variable aleatoire de densite f, alors que f depend 
# d'un parametre C non trivial a obtenir